from skyfield.api import EarthSatellite, utc, load
from skyfield.api import utc, wgs84

import ephem

import requests

from datetime import timedelta, datetime
import time
import math

TLE = requests.get("https://db.satnogs.org/api/tle/").json()[0] #This is the ISS TLE from DB

stationLat = 41.814
stationLng = -87.709
stationElev = 181

timestep = timedelta(days=5)


startDate = datetime.now(tz=utc)
endDate = startDate + timestep

def Skyfield_pass_predictions(tle,start_date,end_date,lat,lng,elev):
    print("Generating Skyfield Predictions")
    start = time.time()
    predictions = []
    satellite = EarthSatellite(tle["tle1"], tle["tle2"],tle["tle0"])
    station = wgs84.latlon(lat,lng,elevation_m=elev)
    ts = load.timescale()
    t, events = satellite.find_events(station, ts.from_datetime(start_date), ts.from_datetime(end_date),altitude_degrees=-1)
    pre_predict = {"start":ts.from_datetime(start_date).utc_strftime("%Y-%m-%d %H:%M:%S.%f"),"end":ts.from_datetime(end_date).utc_strftime("%Y-%m-%d %H:%M:%S.%f")}
    for ti, event in zip(t, events):
        if event == 0:
            pre_predict["start"] = ti.utc_datetime()
        elif event == 2:
            pre_predict["end"] = ti.utc_datetime()
            predictions.append(pre_predict.copy())
        
    if event == 1:
        pre_predict["end"] = end_date
        predictions.append(pre_predict.copy())
    
    end = time.time()
    return predictions,start,end

def makeAware(dt):
    dt = dt.replace(tzinfo=utc)
    return dt

def pyephem_pass_predictions(tle,start_date,end_date,lat,lng,elev):
    print("Generating Pyephem Predictions")
    start = time.time()
    predictions = []
    satellite = ephem.readtle(tle["tle0"], tle["tle1"],tle["tle2"])
    time_start_new = start_date
    observer = ephem.Observer()
    observer.lon = str(lng)
    observer.lat = str(lat)
    observer.elevation = elev
    observer.date = time_start_new.strftime("%Y-%m-%d %H:%M:%S.%f")

    while time_start_new <= end_date:
        pass_params = {}
        rise_time, rise_az, tca_time, tca_alt, set_time, set_az = observer.next_pass(satellite, True)
        pass_params["rise_time"] = makeAware(rise_time.datetime())
        pass_params["set_time"] = makeAware(set_time.datetime())
        if pass_params['rise_time'] >= end_date:
            # start of next pass outside of window bounds
            break

        if pass_params['set_time'] > end_date:
            # end of next pass outside of window bounds
            pass_params['set_time'] = end_date


        time_start_new = pass_params['set_time'] + timedelta(minutes=1)
        observer.date = time_start_new.strftime("%Y-%m-%d %H:%M:%S.%f")
        predictions.append({"start":pass_params['rise_time'],"end":pass_params['set_time']})
    end = time.time()
    return predictions,start,end



skyfield_passes,skyfield_start,skyfield_end = Skyfield_pass_predictions(TLE,startDate,endDate,stationLat,stationLng,stationElev)
pyephem_passes,pyephem_start,pyephem_end = pyephem_pass_predictions(TLE,startDate,endDate,stationLat,stationLng,stationElev)

print("")
print("Skyfield run time",skyfield_end-skyfield_start)
print("")
print("pyephem run time for",pyephem_end-pyephem_start)
print("")
print("Skyfield found ",len(skyfield_passes),"passes. pyephem found ",len(pyephem_passes),"passes.")
print("Listing ",min(len(skyfield_passes),len(pyephem_passes)), "passes.")
print("")

print("Start Date",startDate.strftime("%Y-%m-%d %H:%M:%S.%f"))
print("End Date",endDate.strftime("%Y-%m-%d %H:%M:%S.%f"))
print("")

rise_deltas = []
fall_deltas = []
for x in range(min(len(skyfield_passes),len(pyephem_passes))):
    print("Skyfield Pass #",x)
    print("Rise",skyfield_passes[x]["start"])
    print("Fall",skyfield_passes[x]["end"])
    print("")
    print("pyephem Pass #",x)
    print("Rise",pyephem_passes[x]["start"])
    print("Fall",pyephem_passes[x]["end"])
    print("")
    print("Rise Delta",abs(skyfield_passes[x]["start"]-pyephem_passes[x]["start"]))
    print("Fall Delta",abs(skyfield_passes[x]["end"]-pyephem_passes[x]["end"]))
    print("\n")

    rise_deltas.append(abs(skyfield_passes[x]["start"]-pyephem_passes[x]["start"]))
    fall_deltas.append(abs(skyfield_passes[x]["end"]-pyephem_passes[x]["end"]))
    
print("Max time delta for rise")
print(max(rise_deltas))
print("Max time delta for fall")
print(max(fall_deltas))


