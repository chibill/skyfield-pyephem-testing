from skyfield.api import EarthSatellite, utc, load
from skyfield.api import utc

import ephem

import requests

from datetime import timedelta, datetime
import time
import math

TLE = requests.get("https://db.satnogs.org/api/tle/").json()[0] #This is the ISS TLE from DB


timestep = timedelta(seconds=1)
count = 100000

check_times = []
timestamp = datetime.now(tz=utc)
for x in range(count):
    check_times.append(timestamp)
    timestamp = timestamp + timestep



def Skyfield_gen_predictions(tle,times):
    print("Generating Skyfield Predictions")
    start = time.time()
    predictions = {}
    satellite = EarthSatellite(tle["tle1"], tle["tle2"],tle["tle0"])
    ts = load.timescale()
    for x in times:
        subpoint = satellite.at(ts.from_datetime(x)).subpoint()
        lat = subpoint.latitude.radians
        lng = subpoint.longitude.radians
        predictions[x.timestamp()] = (lat,lng)
    end = time.time()
    return predictions,start,end

def pyephem_gen_predictions(tle,times):
    print("Generating Pyephem Predictions")
    start = time.time()
    predictions = {}
    satelite = ephem.readtle(tle["tle0"], tle["tle1"],tle["tle2"])
    for x in times:
        satelite.compute(x.strftime('%Y-%m-%d %H:%M:%S.%f'))
        lat = satelite.sublat
        lng = satelite.sublong 
        predictions[x.timestamp()] = (lat,lng)
    end = time.time()
    return predictions,start,end
    
    
skyfield_predictions, skyfield_start,skyfield_end = Skyfield_gen_predictions(TLE,check_times)
#print(skyfield_predictions, skyfield_end-skyfield_start)

pyephem_predictions, pyephem_start,pyephem_end = pyephem_gen_predictions(TLE,check_times)
#print(pyephem_predictions, pyephem_end-pyephem_start)

print("")
print("Skyfield run time for",count,"timesteps",skyfield_end-skyfield_start,"s")
print("Time per step is about",(skyfield_end-skyfield_start)/count,"s")
print("")
print("pyephem run time for",count,"timesteps",pyephem_end-pyephem_start,"s")
print("Time per steps is about",(pyephem_end-pyephem_start)/count,"s")
print("")

diffs = []
for x in check_times:
    timestamp = x.timestamp()
    diffs.append(abs(math.dist(skyfield_predictions[timestamp],pyephem_predictions[timestamp])))
    
print("Max orbital position diff",max(diffs),"radians")
